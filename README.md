# Master Thesis 

This is my Master Thesis titled 'Causal Effects in Black Hole Scattering to Third Post-Minkowskian Order'

# Abstract

We compute the total change of momentum, including conservative and dissipative effects of an unbound black hole encounter up to NNLO in $G$. The in-in formalism is introduced and the corresponding Feynman rules are derived in a post Minkowskian effective field theory framework. A diagrammatic approach is used to construct the effective action, which leads to the change in momentum in-integrand. The integrals are bootstrapped via differential equations and retarded propagators are treated via consistency relations, or directly using cutting rules. Secondly, `WoLF` is introduced. It is a fast and memory efficient integrand construction package written and used in `C++`. 

# Usage

Simply download 'ma-jakob-neef.pdf' and enjoy reading.

Alternatively you can read the paper https://arxiv.org/abs/2207.00580 which publishes the results.

The slides to the thesis' defense can be found in  https://jakobneef.gitlab.io/master-defense



